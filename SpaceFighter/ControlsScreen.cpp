#include "ControlsScreen.h"
#include "MainMenuScreen.h"

void OnBackSelect(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new MainMenuScreen());
}

ControlsScreen::ControlsScreen()
{
	m_pTexture = nullptr;
	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);
	Show();
}

void ControlsScreen::LoadContent(ResourceManager* pResourceManager)
{
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	const int COUNT = 10;
	MenuItem* pItem;
	Font::SetLoadSize(20, true);
	Font* pFont = pResourceManager->Load<Font>("Fonts\\Ethnocentric.ttf");

	SetDisplayCount(COUNT);

	enum Items { KEYBOARD, LEFT, RIGHT, UP, DOWN, SHOOT, XBOX, MOVE, FIRE, BACK,};
	std::string text[COUNT] = { "Keyboard", "Move Left = Left Arrow Key", "Move Right = Right Arrow Key", "Move Up = Up Arrow Key", "Move Down = Down Arrow Key", 
		"Shoot = Spacebar", "XBOX Controller", "Move = Left Joystick", "Shoot = Right Trigger", "Main Menu" };

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(100, 400 + 50 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Blue);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}
	GetMenuItem(BACK)->SetSelectCallback(OnBackSelect);
}



void ControlsScreen::Update(const GameTime* pGameTime)
{
	MenuItem* pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		//Changed Color of menu item list
		if (pItem->IsSelected()) pItem->SetColor(Color::Red);
		else if (i == 0 || i == 6 || i == 9)
			pItem->SetColor(Color::Blue);
		else 
			pItem->SetColor(Color::Green);
	}
	MenuScreen::Update(pGameTime);
}

void ControlsScreen::Draw(SpriteBatch* pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);

}