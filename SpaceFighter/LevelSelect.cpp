#include "LevelSelect.h"
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

void OnLevel1Select(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen(0));
}
void OnLevel2Select(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen(1));
}
void OnLevel3Select(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen(2));
}

LevelSelect::LevelSelect()
{
	m_pTexture = nullptr;

	SetTransitionInTime(2.0f);
	SetTransitionOutTime(0.5f);

	Show();
}

void LevelSelect::LoadContent(ResourceManager* pResourceManager)
{
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	// Create the menu items
	const int COUNT = 3;
	MenuItem *pItem;
	Font::SetLoadSize(20, true);
	Font *pFont = pResourceManager->Load<Font>("Fonts\\Ethnocentric.ttf");

	SetDisplayCount(COUNT);

	enum Items { ONE, TWO, THREE };
	std::string text[COUNT] = { "One", "Two", "Three" };

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(100, 100 + 50 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Blue);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}
	GetMenuItem(ONE)->SetSelectCallback(OnLevel1Select);
	GetMenuItem(TWO)->SetSelectCallback(OnLevel2Select);
	GetMenuItem(THREE)->SetSelectCallback(OnLevel3Select);
}

void LevelSelect::Update(const GameTime* pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::Red);
		else pItem->SetColor(Color::Green);
	}
	MenuScreen::Update(pGameTime);
}

void LevelSelect::Draw(SpriteBatch* pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}