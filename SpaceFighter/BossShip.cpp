
#include "BossShip.h"


BossShip::BossShip()
{
	SetSpeed(50);
	SetMaxHitPoints(10);
	SetCollisionRadius(40);
}

void BossShip::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}

void BossShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::Red, m_pTexture->GetCenter(), Vector2::THREE, Math::PI, 1);
	}
}