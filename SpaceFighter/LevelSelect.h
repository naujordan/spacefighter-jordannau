#pragma once

#include "KatanaEngine.h"

using namespace KatanaEngine;

class LevelSelect : public MenuScreen
{

public:

	LevelSelect();

	virtual ~LevelSelect() { }

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;
	Vector2 m_texturePosition;


};