
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"
#include "ControlsScreen.h"
#include "LevelSelect.h"

// Callback Functions
void OnStartGameSelect(MenuScreen *pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen(0));
}

void OnQuitSelect(MenuScreen *pScreen)
{
	//sets pointer to main menu screen
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	//Sets quitting flag tracker to true
	pMainMenuScreen->SetQuitFlag();
	//Calls exit from katana to exit the game
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}

void OnControlsSelect(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new ControlsScreen());
}

void OnLevelSelect(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new LevelSelect());
}



MainMenuScreen::MainMenuScreen()
{
	m_pTexture = nullptr;

	SetRemoveCallback(OnScreenRemove);

	//2 second load in time
	SetTransitionInTime(2.0f);
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	// Create the menu items
	const int COUNT = 4;
	MenuItem *pItem;
	Font::SetLoadSize(20, true);
	//Changed Font
	Font *pFont = pResourceManager->Load<Font>("Fonts\\Ethnocentric.ttf");

	SetDisplayCount(COUNT);

	enum Items { START_GAME, CONTROLS, LEVEL_SELECT, QUIT};
	std::string text[COUNT] = { "Start Game", "Controls", "Level Select", "Quit"};

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(100, 100 + 50 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Blue);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}

	GetMenuItem(START_GAME)->SetSelectCallback(OnStartGameSelect);
	GetMenuItem(CONTROLS)->SetSelectCallback(OnControlsSelect);
	GetMenuItem(LEVEL_SELECT)->SetSelectCallback(OnLevelSelect);
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect);
	
}

void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		//Changed Color of menu item list
		if (pItem->IsSelected()) pItem->SetColor(Color::Red);
		else pItem->SetColor(Color::Green);
	}

	MenuScreen::Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}
